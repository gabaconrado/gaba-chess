import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;


/**
 * Write a description of class Location here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Location implements MouseListener{
    // instance variables - replace the example below with your own
    private int x;
    private int y;
    private JPanel panel;
    private Piece p;
    private static boolean moving = false;
    private static Location temp;
    private PieceManager manager;

    /**
     * Constructor for objects of class Location
     */
    
    public boolean isEmpty(){
        return ( p == null );
    }
    
    public Location(){
        // initialise instance variables
        x = 0;
        y = 0;
        panel = new JPanel();
        
    }
    
    public Piece getPiece(){
        return this.p;
    }
    
    public void setPiece(Piece p){
        this.p = p;
    }
    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public Location(int x, int y, Piece p, PieceManager m){
       
       this.setLocation(x,y);
       this.p = p;
       this.manager = m;
       
       panel = new JPanel();
       panel.addMouseListener(this);
       
       repaintImage(panel, p);
       
    }
    
    public void repaintImage(JPanel panel, Piece p){
        if (p != null){
           BufferedImage myPicture = null;
           try{
               myPicture = ImageIO.read(new File(p.getImageName()));
           }catch(IOException e){
               e.printStackTrace();
           }
           if(myPicture != null){
               JLabel picLabel = new JLabel(new ImageIcon(myPicture));
               panel.add(picLabel);
               panel.revalidate();
               panel.repaint();
           }
       }
    }

    public int getX(){
        return this.x;
    }
    
    public void setX(int x){
        this.x = x;
    }
    
    public int getY(){
        return this.y;
    }
    
    public void setY(int y){
        this.y = y;
    }
    
    public void setLocation(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public JPanel getPanel(){
        return panel;
    }
    
    public void setPanel(JPanel p){
        this.panel = p;
    }
    
    
    
    public void mousePressed(MouseEvent e) {
        
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        if(!moving){
            temp = this;
            moving = true;
        }
        else{
            manager.move(temp, this);
            moving = false;
            temp = null;
        }
    }
    
    
}
