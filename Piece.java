
/**
 * Write a description of class Piece here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Piece
{
    private String imageName;

    /**
     * Constructor for objects of class Piece
     */
    public Piece(){
        // initialise instance variables
    }
    
    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public Piece(String imageName){
        this.imageName = imageName;
    }
    
    public String getImageName(){
        return imageName;
    }
    
    public void setImageName(String i){
        this.imageName = i;
    }
}
