import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JMenuItem;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Write a description of class Game here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Game extends JFrame
{
    private JPanel windowPanel, boardPanel, barPanel;
    private Location[][] board;
    private PieceManager manager;
    private boolean moving;
    private boolean turn;
    private Location temp;
    
    /**
     * Constructor for objects of class Game
     */
    public Game()
    {
        super("GabaChess");
        setWindow();
        this.setSize(800,600);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
        this.setVisible(true);
    }
        
    public void setWindow(){
        
        setMenu();
        setBoard();
        setBar();
        
        windowPanel = new JPanel();
        windowPanel.setLayout(new BorderLayout());
        
        windowPanel.add(boardPanel, BorderLayout.CENTER);
        windowPanel.add(barPanel, BorderLayout.SOUTH);
        
        this.add(windowPanel);
        this.pack();
        
        
    }
    
    public void setMenu(){
        
        JMenuBar menuBar = new JMenuBar();
        JMenu menuGame, menuOptions, menuAbout;
        JMenuItem itemStart, itemClose;
        
        menuGame = new JMenu("Game");
        menuOptions = new JMenu("Options");
        menuOptions.addMouseListener(new MouseAdapter(){
            
            public void mousePressed(MouseEvent e) {
        
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }

            public void mouseClicked(MouseEvent e) {
                JOptionPane.showMessageDialog(null, "Not implemented yet.", "Options", JOptionPane.WARNING_MESSAGE);
            }
            
        });
        
        menuAbout = new JMenu("About");
        menuAbout.addMouseListener(new MouseAdapter(){
            
            public void mousePressed(MouseEvent e) {
        
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }

            public void mouseClicked(MouseEvent e) {
                JOptionPane.showMessageDialog(null, "Made by Gabriel Conrado @ 2015.", "About", JOptionPane.INFORMATION_MESSAGE);
            }
            
        });
        
        itemStart = new JMenuItem("Start Game");
        itemStart.addActionListener(new ActionListener(){
            
            public void actionPerformed(ActionEvent e){
                restartGame();
            }
            
        });
        
        itemClose = new JMenuItem("Close Game");
        itemClose.addActionListener(new ActionListener(){
            
            public void actionPerformed(ActionEvent e){
                System.exit(0);
            }
            
        });
        
        menuGame.add(itemStart);
        menuGame.add(itemClose);
        
        menuBar.add(menuGame);
        menuBar.add(menuOptions);
        menuBar.add(menuAbout);
    
        this.setJMenuBar(menuBar);
        
    }
    
    public void setBoard(){
        
        boardPanel = new JPanel();
        boardPanel.setLayout(new GridLayout(8,8));        
        restartGame();
    
    }
    
    public void restartGame(){
        int x,y;
        this.manager = new PieceManager();
        boardPanel.removeAll();
        board = manager.getBoard();
        for(x = 0; x < 8; x++){            
            for(y = 0; y < 8; y++){                            
                boardPanel.add(board[x][y].getPanel());                
            }            
        }
        boardPanel.revalidate();
        boardPanel.repaint();
    }
    
    public void setBar(){
        
        JButton btCheckMate = new JButton("Claim CheckMate");
        btCheckMate.addActionListener(new ActionListener(){
            
            public void actionPerformed(ActionEvent e){
                int n = JOptionPane.showConfirmDialog(null, "Are you sure it is a CheckMate ?", "Checkmate ?", JOptionPane.YES_NO_OPTION);
                if(n == JOptionPane.YES_OPTION){
                    finishGame(1);
                }
            }
            
        });
        JButton btDraw      = new JButton("Claim a Draw");
        btDraw.addActionListener(new ActionListener(){
            
            public void actionPerformed(ActionEvent e){
                int n = JOptionPane.showConfirmDialog(null, "Are you sure it is a Draw ?", "Draw ?", JOptionPane.YES_NO_OPTION);
                if(n == JOptionPane.YES_OPTION){
                    finishGame(2);
                }
            }
            
        });
        
        barPanel = new JPanel();
        barPanel.setLayout(new FlowLayout());
        barPanel.add(btCheckMate);
        barPanel.add(btDraw);
    
    }

    public void finishGame(int i){
        
        this.setJMenuBar(null);
        windowPanel.removeAll();
        BufferedImage myPicture = null;
        String imageName;   
        if(i == 1)
            imageName = "victory.png";
        else
            imageName = "draw.png";
        try{
               myPicture = ImageIO.read(new File(imageName));
           }catch(IOException e){
               e.printStackTrace();
        }
        if(myPicture != null){
               JLabel picLabel = new JLabel(new ImageIcon(myPicture));
               windowPanel.add(picLabel);
               windowPanel.revalidate();
               windowPanel.repaint();
           }
        
    }
    
}
