import java.util.ArrayList;
import java.util.List;
import java.awt.Color;
/**
 * Write a description of class PieceManager here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PieceManager{
    
    private Team white;
    private Team black;
    private List<Location> places;
    
    // instance variables - replace the example below with your own
    public PieceManager(){
        
        places = new ArrayList<Location>();
        createTeams();
        populateTeams();
        placePieces();
        
    }

    public void placePieces(){
        
        int i, y;
        y=0;
        for(i=0;i<8;i++){
            places.add(new Location(1, i, white.getPieces().get(y), this));
            places.add(new Location(6, i, black.getPieces().get(y), this));
            y++;
        }
        for(i=0;i<2;i++){
            places.add(new Location(0, i*7, white.getPieces().get(y), this));
            places.add(new Location(7, i*7, black.getPieces().get(y), this));
            y++;
        }
        for(i=1;i<3;i++){
            places.add(new Location(0, i, white.getPieces().get(y), this));
            places.add(new Location(7, i, black.getPieces().get(y), this));
            y++;
        }
        for(i=6;i>4;i--){
            places.add(new Location(0, i, white.getPieces().get(y), this));
            places.add(new Location(7, i, black.getPieces().get(y), this));
            y++;
        }
        places.add(new Location(0,3, white.getPieces().get(y), this));
        places.add(new Location(7,4, black.getPieces().get(y), this));
        y++;
        places.add(new Location(0,4, white.getPieces().get(y), this));
        places.add(new Location(7,3, black.getPieces().get(y), this));
    }
    
    public Location[][] getBoard(){
        
        Location[][] board = new Location[8][8];        
       
        int x,y;
        boolean colorFlag = true;
        
        for(Location l : places){
            board[l.getX()][l.getY()] = l;
        }
        
        for(x=0; x<8; x++){
            for(y=0; y<8; y++){
                if(board[x][y] == null)
                    board[x][y] = new Location(x, y, null, this);
                if(colorFlag)
                    board[x][y].getPanel().setBackground(Color.WHITE);
                else
                    board[x][y].getPanel().setBackground(Color.BLACK); 
                colorFlag = !colorFlag;
            }
            colorFlag = !colorFlag;
        }
        
        return board;
        
    }
    
    public void createTeams(){
        
        white = new Team(0);
        black = new Team(1);
    
    }
    
    public void populateTeams(){
        
        int i;
        for(i = 0; i < 8; i++){
            white.getPieces().add(new Pawn(true));
            black.getPieces().add(new Pawn(false));
        }
        for(i = 0; i < 2; i++){
            white.getPieces().add(new Tower(true));
            black.getPieces().add(new Tower(false));
        }
        for(i = 0; i < 4; i++){
            white.getPieces().add(new Horse(true));
            black.getPieces().add(new Horse(false));
        }
        white.getPieces().add(new Queen(true));
        black.getPieces().add(new Queen(false));
        white.getPieces().add(new King(true));
        black.getPieces().add(new King(false));
    }
    
    public void move(Location o, Location n){
        
        Location aux;        
        Piece p = o .getPiece();        
        if(n.getPiece() != null){
            n.getPanel().removeAll();
        }
        n.repaintImage(n.getPanel(), p);        
        n.setPiece(p);
        
        o.getPanel().removeAll();
        o.getPanel().repaint();
        o.setPiece(null);
             
    }
}
