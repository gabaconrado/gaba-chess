import java.util.List;
import java.util.ArrayList;
/**
 * Write a description of class Team here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Team
{
    
    // instance variables - replace the example below with your own
    
    private List<Piece> pieces;
    int id;
    
    /**
     * Constructor for objects of class Team
     */
    public Team(int id){
        
       this.id = id;
       pieces = new ArrayList<Piece>();
       
    }
    
    public void createPiece(Piece p){
        
        this.pieces.add(p);
        
    }
    
    public List<Piece> getPieces(){
        
        return pieces;
        
    }


}
